<?php

namespace Service;

use Horeca\MiddlewareCommonLib\Service\HorecaApiService;
use JMS\Serializer\SerializerBuilder;
use PHPUnit\Framework\TestCase;

class HorecaApiServiceTest extends TestCase
{
    private HorecaApiService $horecaApi;

    protected function setUp(): void
    {
        $serializer = SerializerBuilder::create()->build();
        $this->horecaApi = new HorecaApiService($serializer);
    }
}
