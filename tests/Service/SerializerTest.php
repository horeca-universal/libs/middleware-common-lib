<?php

namespace Service;

use Horeca\MiddlewareCommonLib\Model\Cart\ShoppingCart;
use Horeca\MiddlewareCommonLib\Model\Protocol\ShoppingCartProductMappingsResponse;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use PHPUnit\Framework\TestCase;

class SerializerTest extends TestCase
{
    private SerializerInterface $serializer;

    protected function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function test_deserializeShoppingCart()
    {
        try {
            // Full example of a ShoppingCart object containing a table order
            $order = '{"restaurant":{"id":"b616c748-ba13-11eb-8251-005056913825","name":"Demo Restaurant","website":"Demo Restaurant"},"id":"b0d84ebb-17bd-11ec-b681-005056913825","order_no":"H3XYL5FJ","time":"2021-09-17 16:46:42","total_price_before_discount":114,"total_price":114,"delivery_cost":0,"status":"done","payment_status":"","customer":{"id":"b0d85734-17bd-11ec-b681-005056913825","first_name":"Masa","last_name":"10","email":"1631886402@greatfood.ro"},"entries":[{"id":"b0d86070-17bd-11ec-b681-005056913825","quantity":1,"final_price":8,"full_name":"Cappucino","type":"normal","options":[]},{"id":"b0d86e09-17bd-11ec-b681-005056913825","quantity":1,"final_price":26,"full_name":"Snitel De Porc (pane)","type":"normal","options":[]},{"id":"b0d876d8-17bd-11ec-b681-005056913825","quantity":1,"final_price":23,"full_name":"Ceafa De Porc Grill","type":"configurable","options":[{"id":"b0d8a784-17bd-11ec-b681-005056913825","default_quantity":0,"free_quantity":0,"entry":{"id":"b0d8afe7-17bd-11ec-b681-005056913825","quantity":0,"final_price":0,"full_name":"Metoda De Preparare","type":"group","options":[{"id":"b0d8b773-17bd-11ec-b681-005056913825","default_quantity":0,"free_quantity":0,"entry":{"id":"b0d8be1f-17bd-11ec-b681-005056913825","quantity":1,"final_price":0,"full_name":"Rare","type":"normal","options":[]},"position":1}]},"position":2}]}],"invoice_type":"person","delivery_method":"local-table","delivery_time_choice":"now","payment_method":"cash","estimated_delivery_time":"2021-10-29 11:38:25","cutlery_quantity":0,"shopping_cart_tips":[{"id":"735571d4-17b8-11ec-b681-005056913822","amount":1.5,"payment_status":"success"}],"table":{"id":"735571d4-17b8-11ec-b681-005056913825","number":"10"}}';
            $cart = $this->serializer->deserialize($order, ShoppingCart::class, 'json');
            $this->assertInstanceOf(ShoppingCart::class, $cart);
            $this->serializer->serialize($cart, 'json');
        } catch (\Exception $e) {
            $this->fail('Deserialize TableOrder Error: ' . $e->getMessage());
        }

        try {
            // Delivery order
            $order = '{"restaurant":{"id":"b616c748-ba13-11eb-8251-005056913825","name":"Demo Restaurant","website":"Demo Restaurant"},"delivery_zone":{"id":"a73f7d56-becc-11eb-8251-005056913825","name":"Demo-restaurant-centru"},"id":"603f1134-478e-11ec-999f-005056913825","order_no":"FCRX2ZA3","time":"2021-11-17 12:09:57","total_price_before_discount":20,"total_price":20,"delivery_cost":0,"status":"done","payment_status":"","customer":{"phone_number":"0736466774","id":"57ac5629-becd-11eb-8251-005056913825","first_name":"Gabriel","last_name":"Cristea","email":"gcristea07@gmail.com"},"delivery_address":{"string_representation":"Strada Sepcari, Nr. 1, Reper Test, Bucuresti","city_name":"Bucuresti","county_name":"Bucuresti","id":"84980e96-478e-11ec-999f-005056913825","street":"strada sepcari","number":"1","building":"","stairs":"","apartment":"","floor":"","interphone":"","latitude":44.4306829,"longitude":26.1032704,"info":"test"},"entries":[{"id":"7dca759e-478e-11ec-999f-005056913825","quantity":1,"final_price":20,"full_name":"Pizza Philadelphia 32 Cm","type":"configurable","options":[]}],"invoice_type":"person","delivery_method":"delivery","delivery_time_choice":"now","payment_method":"cash","estimated_delivery_time":"2021-12-06 13:36:20","estimated_preparation_time":"2021-12-06 13:36:20","cutlery_quantity":0,"shopping_cart_tips":[]}';
            $cart = $this->serializer->deserialize($order, ShoppingCart::class, 'json');
            $this->assertInstanceOf(ShoppingCart::class, $cart);
            $this->serializer->serialize($cart, 'json');
        } catch (\Exception $e) {
            $this->fail('Deserialize DiscountOrder Error: ' . $e->getMessage());
        }
    }

    public function test_deserializeProductMapping()
    {
        $payload = '{"mappings":[{"internal_product_id":"some-id-10","external_product_id":"some-id-11","shopping_cart_entry_id":"some-id-12"},{"internal_product_id":"some-id-20","external_product_id":"some-id-21","shopping_cart_entry_id":"some-id-22"}]}';

        /** @var ShoppingCartProductMappingsResponse $data */
        $data = $this->serializer->deserialize($payload, ShoppingCartProductMappingsResponse::class, 'json');

        $this->assertCount(2, $data->getMappings(), 'Invalid deserialization');
        $this->assertEquals("some-id-10", $data->getMappings()[0]->getInternalProductId());
        $this->assertEquals("some-id-11", $data->getMappings()[0]->getExternalProductId());
        $this->assertEquals("some-id-12", $data->getMappings()[0]->getShoppingCartEntryId());

        $this->assertEquals("some-id-20", $data->getMappings()[1]->getInternalProductId());
        $this->assertEquals("some-id-21", $data->getMappings()[1]->getExternalProductId());
        $this->assertEquals("some-id-22", $data->getMappings()[1]->getShoppingCartEntryId());
    }
}
