<?php

namespace Horeca\MiddlewareCommonLib\Model\Restaurant;

use JMS\Serializer\Annotation as Serializer;

class RestaurantTable
{
    /**
     * @Serializer\SerializedName("id")
     * @Serializer\Type("string")
     */
    private string $id;

    /**
     * @Serializer\SerializedName("number")
     * @Serializer\Type("string")
     */
    private string $tableNumber;

    //<editor-fold desc="Getters & Setters">

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getTableNumber(): string
    {
        return $this->tableNumber;
    }

    public function setTableNumber(string $tableNumber): void
    {
        $this->tableNumber = $tableNumber;
    }

    //</editor-fold>
}
