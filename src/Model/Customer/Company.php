<?php

namespace Horeca\MiddlewareCommonLib\Model\Customer;

use JMS\Serializer\Annotation as Serializer;

class Company
{
    /**
     * @Serializer\SerializedName("id")
     * @Serializer\Type("string")
     */
    private string $id;

    /**
     * @Serializer\SerializedName("name")
     * @Serializer\Type("string")
     */
    private string $name;

    /**
     * @Serializer\SerializedName("identification_code")
     * @Serializer\Type("string")
     */
    private string $identificationCode;

    /**
     * @Serializer\SerializedName("vat_code")
     * @Serializer\Type("string")
     */
    private string $vatCode;

    /**
     * @Serializer\SerializedName("trade_registry_number")
     * @Serializer\Type("string")
     */
    private string $tradeRegistryNumber;

    /**
     * @Serializer\SerializedName("address")
     * @Serializer\Type("string")
     */
    private ?string $address = null;

    /**
     * @Serializer\SerializedName("city")
     * @Serializer\Type("string")
     */
    private ?string $city = null;

    /**
     * @Serializer\SerializedName("state")
     * @Serializer\Type("string")
     */
    private ?string $state = null;
    /**
     * @Serializer\SerializedName("country")
     * @Serializer\Type("string")
     */
    private ?string $country = null;


    //<editor-fold desc="Getters & Setters">

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getIdentificationCode(): string
    {
        return $this->identificationCode;
    }

    public function setIdentificationCode(string $identificationCode): void
    {
        $this->identificationCode = $identificationCode;
    }

    public function getVatCode(): string
    {
        return $this->vatCode;
    }

    public function setVatCode(string $vatCode): void
    {
        $this->vatCode = $vatCode;
    }

    public function getTradeRegistryNumber(): string
    {
        return $this->tradeRegistryNumber;
    }

    public function setTradeRegistryNumber(string $tradeRegistryNumber): void
    {
        $this->tradeRegistryNumber = $tradeRegistryNumber;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): void
    {
        $this->state = $state;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }



    //</editor-fold>
}
