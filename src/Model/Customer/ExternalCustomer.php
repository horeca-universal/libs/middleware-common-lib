<?php

namespace Horeca\MiddlewareCommonLib\Model\Customer;

use JMS\Serializer\Annotation as Serializer;

class ExternalCustomer
{
    /**
     * @Serializer\SerializedName("id")
     * @Serializer\Type("string")
     */
    protected ?string $id;

    /**
     * @Serializer\SerializedName("name")
     * @Serializer\Type("string")
     */
    private ?string $name = null;

    /**
     * @Serializer\SerializedName("phone_number")
     * @Serializer\Type("string")
     */
    protected string $phoneNumber;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     */
    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber(string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }




}
