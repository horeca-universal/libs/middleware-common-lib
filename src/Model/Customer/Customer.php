<?php

namespace Horeca\MiddlewareCommonLib\Model\Customer;

use JMS\Serializer\Annotation as Serializer;

class Customer
{
    /**
     * @Serializer\SerializedName("id")
     * @Serializer\Type("string")
     */
    private string $id;

    /**
     * @Serializer\SerializedName("first_name")
     * @Serializer\Type("string")
     */
    private ?string $firstName = null;

    /**
     * @Serializer\SerializedName("last_name")
     * @Serializer\Type("string")
     */
    private ?string $lastName = null;

    /**
     * @Serializer\SerializedName("email")
     * @Serializer\Type("string")
     */
    private ?string $email = null;

    /**
     * @Serializer\SerializedName("phone_number")
     * @Serializer\Type("string")
     */
    private ?string $phoneNumber = null;

    //<editor-fold desc="Getters & Setters">

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getFullName(): string
    {
        return sprintf('%s %s', $this->firstName, $this->lastName);
    }

    //</editor-fold>
}
