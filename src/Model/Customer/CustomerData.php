<?php

namespace Horeca\MiddlewareCommonLib\Model\Customer;

class CustomerData
{

    public $name;
    public $phoneNumber;

    public function __construct($name, $phoneNumber){
        $this->phoneNumber = $phoneNumber;
        $this->name = $name;

    }

}
