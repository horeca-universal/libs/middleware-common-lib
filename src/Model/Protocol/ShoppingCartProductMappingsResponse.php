<?php

namespace Horeca\MiddlewareCommonLib\Model\Protocol;

use Horeca\MiddlewareCommonLib\Model\Product\ProductMapping;
use JMS\Serializer\Annotation as Serializer;

class ShoppingCartProductMappingsResponse
{

    /**
     * @Serializer\SerializedName("mappings")
     * @Serializer\Type("array<Horeca\MiddlewareCommonLib\Model\Product\ProductMapping>")
     * @var ProductMapping[]|array
     */
    private array $mappings = [];

    public function getMappings(): array
    {
        return $this->mappings;
    }

    public function setMappings(array $mappings): void
    {
        $this->mappings = $mappings;
    }

}
