<?php

namespace Horeca\MiddlewareCommonLib\Model\Protocol;

use JMS\Serializer\Annotation as Serializer;

class SendShoppingCartResponse
{

    /**
     * @Serializer\SerializedName("horeca_order_id")
     * @Serializer\Type("string")
     */
    public string $horecaOrderId;


}
