<?php

namespace Horeca\MiddlewareCommonLib\Model\Product;

use JMS\Serializer\Annotation as Serializer;

class ProductMapping
{
    /**
     * @Serializer\SerializedName("shopping_cart_entry_id")
     * @Serializer\Type("string")
     */
    private string $shoppingCartEntryId;

    /**
     * @Serializer\SerializedName("internal_product_id")
     * @Serializer\Type("string")
     */
    private string $internalProductId;

    /**
     * @Serializer\SerializedName("external_product_id")
     * @Serializer\Type("string")
     * @var string|int
     */
    private $externalProductId;

    //<editor-fold desc="Getters & Setters">

    public function getShoppingCartEntryId(): string
    {
        return $this->shoppingCartEntryId;
    }

    public function setShoppingCartEntryId(string $shoppingCartEntryId): void
    {
        $this->shoppingCartEntryId = $shoppingCartEntryId;
    }

    public function getInternalProductId(): string
    {
        return $this->internalProductId;
    }

    public function setInternalProductId(string $internalProductId): void
    {
        $this->internalProductId = $internalProductId;
    }

    public function getExternalProductId()
    {
        return $this->externalProductId;
    }

    public function setExternalProductId($externalProductId): void
    {
        $this->externalProductId = $externalProductId;
    }

    //</editor-fold>

}
