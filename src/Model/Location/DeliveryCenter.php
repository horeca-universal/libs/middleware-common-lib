<?php

namespace Horeca\MiddlewareCommonLib\Model\Location;

use JMS\Serializer\Annotation as Serializer;

class DeliveryCenter
{
    /**
     * @Serializer\SerializedName("id")
     * @Serializer\Type("string")
     */
    private string $id;

    /**
     * @Serializer\SerializedName("name")
     * @Serializer\Type("string")
     */
    private string $name;

    /**
     * @Serializer\SerializedName("address")
     * @Serializer\Type("string")
     */
    private ?string $address = null;

    /**
     * @Serializer\SerializedName("latitude")
     * @Serializer\Type("float")
     */
    private float $latitude;

    /**
     * @Serializer\SerializedName("longitude")
     * @Serializer\Type("float")
     */
    private float $longitude;

    /**
     * @Serializer\SerializedName("phone_number")
     * @Serializer\Type("string")
     */
    private ?string $phoneNumber = null;

    //<editor-fold desc="Getters & Setters">

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): void
    {
        $this->latitude = $latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): void
    {
        $this->longitude = $longitude;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    //</editor-fold>"

}
