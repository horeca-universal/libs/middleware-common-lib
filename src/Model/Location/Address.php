<?php

namespace Horeca\MiddlewareCommonLib\Model\Location;

use JMS\Serializer\Annotation as Serializer;

class Address
{
    /**
     * @Serializer\SerializedName("id")
     * @Serializer\Type("string")
     */
    private ?string $id = null;

    /**
     * @Serializer\SerializedName("street")
     * @Serializer\Type("string")
     */
    private ?string $streetName = null;

    /**
     * @Serializer\SerializedName("city_name")
     * @Serializer\Type("string")
     */
    private ?string $cityName = null;

    /**
     * @Serializer\SerializedName("country_name")
     * @Serializer\Type("string")
     */
    private ?string $countryName = null;

    /**
     * @Serializer\SerializedName("number")
     * @Serializer\Type("string")
     */
    private ?string $streetNumber = null;

    /**
     * @Serializer\SerializedName("building")
     * @Serializer\Type("string")
     */
    private ?string $building = null;

    /**
     * @Serializer\SerializedName("stairs")
     * @Serializer\Type("string")
     */
    private ?string $stair = null;

    /**
     * @Serializer\SerializedName("apartment")
     * @Serializer\Type("string")
     */
    private ?string $apartment = null;

    /**
     * @Serializer\SerializedName("flooer")
     * @Serializer\Type("string")
     */
    private ?string $floor = null;

    /**
     * @Serializer\SerializedName("interphone")
     * @Serializer\Type("string")
     */
    private ?string $interphone = null;

    /**
     * @Serializer\SerializedName("sector")
     * @Serializer\Type("string")
     */
    private ?string $sector = null;

    /**
     * @Serializer\SerializedName("latitude")
     * @Serializer\Type("float")
     */
    private float $latitude;

    /**
     * @Serializer\SerializedName("longitude")
     * @Serializer\Type("float")
     */
    private float $longitude;

    /**
     * @Serializer\SerializedName("info")
     * @Serializer\Type("string")
     */
    private ?string $info = null;

    /**
     * @Serializer\SerializedName("string_representation")
     * @Serializer\Type("string")
     */
    private string $stringRepresentation;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    public function setStreetName(?string $streetName): void
    {
        $this->streetName = $streetName;
    }

    public function getCityName(): ?string
    {
        return $this->cityName;
    }

    public function setCityName(?string $cityName): void
    {
        $this->cityName = $cityName;
    }

    public function getCountryName(): ?string
    {
        return $this->countryName;
    }

    public function setCountryName(?string $countryName): void
    {
        $this->countryName = $countryName;
    }

    public function getStreetNumber(): ?string
    {
        return $this->streetNumber;
    }

    public function setStreetNumber(?string $streetNumber): void
    {
        $this->streetNumber = $streetNumber;
    }

    public function getBuilding(): ?string
    {
        return $this->building;
    }

    public function setBuilding(?string $building): void
    {
        $this->building = $building;
    }

    public function getStair(): ?string
    {
        return $this->stair;
    }

    public function setStair(?string $stair): void
    {
        $this->stair = $stair;
    }

    public function getApartment(): ?string
    {
        return $this->apartment;
    }

    public function setApartment(?string $apartment): void
    {
        $this->apartment = $apartment;
    }

    public function getFloor(): ?string
    {
        return $this->floor;
    }

    public function setFloor(?string $floor): void
    {
        $this->floor = $floor;
    }

    public function getInterphone(): ?string
    {
        return $this->interphone;
    }

    public function setInterphone(?string $interphone): void
    {
        $this->interphone = $interphone;
    }

    public function getSector(): ?string
    {
        return $this->sector;
    }

    public function setSector(?string $sector): void
    {
        $this->sector = $sector;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): void
    {
        $this->latitude = $latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): void
    {
        $this->longitude = $longitude;
    }

    public function getInfo(): ?string
    {
        return $this->info;
    }

    public function setInfo(?string $info): void
    {
        $this->info = $info;
    }

    public function getStringRepresentation(): string
    {
        return $this->stringRepresentation;
    }

    public function setStringRepresentation(string $stringRepresentation): void
    {
        $this->stringRepresentation = $stringRepresentation;
    }

}
