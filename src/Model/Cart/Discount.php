<?php

namespace Horeca\MiddlewareCommonLib\Model\Cart;

use JMS\Serializer\Annotation as Serializer;

class Discount
{
    /**
     * @Serializer\SerializedName("id")
     * @Serializer\Type("string")
     */
    private string $id;

    /**
     * @Serializer\SerializedName("description")
     * @Serializer\Type("string")
     */
    protected ?string $description;

    /**
     * @Serializer\SerializedName("fixed_value")
     * @Serializer\Type("float")
     */
    protected ?float $fixedValue = null;

    /**
     * @Serializer\SerializedName("percent_value")
     * @Serializer\Type("float")
     */
    protected ?float $percentValue = null;

    /**
     * @Serializer\SerializedName("final_value")
     * @Serializer\Type("float")
     */
    private float $appliedValue;

    /**
     * @Serializer\SerializedName("type")
     * @Serializer\Type("string")
     */
    private ?string $type = null;

    /**
     * @Serializer\SerializedName("discount_code")
     * @Serializer\Type("string")
     */
    private ?string $code = null;

    //<editor-fold desc="Getters & Setters">

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getFixedValue(): ?float
    {
        return $this->fixedValue;
    }

    public function setFixedValue(?float $fixedValue): void
    {
        $this->fixedValue = $fixedValue;
    }

    public function getPercentValue(): ?float
    {
        return $this->percentValue;
    }

    public function setPercentValue(?float $percentValue): void
    {
        $this->percentValue = $percentValue;
    }

    public function getAppliedValue(): float
    {
        return $this->appliedValue;
    }

    public function setAppliedValue(float $appliedValue): void
    {
        $this->appliedValue = $appliedValue;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    //</editor-fold>

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }
}
