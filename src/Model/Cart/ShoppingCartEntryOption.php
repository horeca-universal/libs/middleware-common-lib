<?php

namespace Horeca\MiddlewareCommonLib\Model\Cart;

use JMS\Serializer\Annotation as Serializer;

class ShoppingCartEntryOption
{
    /**
     * @Serializer\SerializedName("id")
     * @Serializer\Type("string")
     */
    private string $id;

    /**
     * @Serializer\SerializedName("default_quantity")
     * @Serializer\Type("integer")
     */
    private int $defaultQuantity = 0;

    /**
     * @Serializer\SerializedName("free_quantity")
     * @Serializer\Type("integer")
     */
    private int $freeQuantity = 0;

    /**
     * @Serializer\SerializedName("entry")
     * @Serializer\Type("Horeca\MiddlewareCommonLib\Model\Cart\ShoppingCartEntry")
     */
    private ShoppingCartEntry $entry;

    /**
     * @Serializer\SerializedName("position")
     * @Serializer\Type("integer")
     */
    private int $position = 0;

    //<editor-fold desc="Getters & Setters">

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getDefaultQuantity(): int
    {
        return $this->defaultQuantity;
    }

    public function setDefaultQuantity(int $defaultQuantity): void
    {
        $this->defaultQuantity = $defaultQuantity;
    }

    public function getFreeQuantity(): int
    {
        return $this->freeQuantity;
    }

    public function setFreeQuantity(int $freeQuantity): void
    {
        $this->freeQuantity = $freeQuantity;
    }

    public function getEntry(): ShoppingCartEntry
    {
        return $this->entry;
    }

    public function setEntry(ShoppingCartEntry $entry): void
    {
        $this->entry = $entry;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    //</editor-fold>
}
