<?php

namespace Horeca\MiddlewareCommonLib\Model\Cart;

use JMS\Serializer\Annotation as Serializer;

class ShoppingCartEntry
{
    /**
     * @Serializer\SerializedName("id")
     * @Serializer\Type("string")
     */
    private string $id;

    /**
     * @Serializer\SerializedName("external_id")
     * @Serializer\Type("string")
     */
    protected $externalId;

    /**
     * @Serializer\SerializedName("quantity")
     * @Serializer\Type("integer")
     */
    private int $quantity = 1;

    /**
     * @Serializer\SerializedName("final_price")
     * @Serializer\Type("float")
     */
    private float $finalPrice;

    /**
     * @Serializer\SerializedName("final_price_per_piece")
     * @Serializer\Type("float")
     */
    private $finalPricePerPiece;

    /**
     * @Serializer\SerializedName("final_price_with_discount")
     * @Serializer\Type("float")
     */
    private ?float $finalPriceWithDiscount = null;

    /**
     * @Serializer\SerializedName("full_name")
     * @Serializer\Type("string")
     */
    private string $name;

    /**
     * @Serializer\SerializedName("type")
     * @Serializer\Type("string")
     */
    private string $type;

    /**
     * @Serializer\SerializedName("options")
     * @Serializer\Type("array<Horeca\MiddlewareCommonLib\Model\Cart\ShoppingCartEntryOption>")
     * @var ShoppingCartEntryOption[]|array
     */
    private array $options = [];

    /**
     * @Serializer\SerializedName("discount")
     * @Serializer\Type("Horeca\MiddlewareCommonLib\Model\Cart\Discount")
     */
    private ?Discount $discount = null;

    /**
     * @Serializer\SerializedName("customer_comment")
     * @Serializer\Type("string")
     */
    private ?string $customerNotes = null;

    /**
     * @Serializer\SerializedName("categoryId")
     * @Serializer\Type("string")
     */
    private string $categoryId;

    /**
     * @Serializer\SerializedName("originalProductId")
     * @Serializer\Type("string")
     */
    private string $productId;

    //<editor-fold desc="Getters & Setters">

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    public function getFinalPrice(): float
    {
        return $this->finalPrice;
    }

    public function setFinalPrice(float $finalPrice): void
    {
        $this->finalPrice = $finalPrice;
    }

    public function getFinalPriceWithDiscount(): ?float
    {
        return $this->finalPriceWithDiscount;
    }

    public function setFinalPriceWithDiscount(?float $finalPriceWithDiscount): void
    {
        $this->finalPriceWithDiscount = $finalPriceWithDiscount;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): void
    {
        $this->options = $options;
    }

    public function getDiscount(): ?Discount
    {
        return $this->discount;
    }

    public function setDiscount(?Discount $discount): void
    {
        $this->discount = $discount;
    }

    public function getCustomerNotes(): ?string
    {
        return $this->customerNotes;
    }

    public function setCustomerNotes(?string $customerNotes): void
    {
        $this->customerNotes = $customerNotes;
    }


    /**
     * @return mixed
     */
    public function getFinalPricePerPiece()
    {
        return $this->finalPricePerPiece;
    }

    /**
     * @param mixed $finalPricePerPiece
     */
    public function setFinalPricePerPiece($finalPricePerPiece): void
    {
        $this->finalPricePerPiece = $finalPricePerPiece;
    }

    /**
     * @return mixed
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param mixed $externalId
     */
    public function setExternalId($externalId): void
    {
        $this->externalId = $externalId;
    }

    public function getCategoryId(): string
    {
        return $this->categoryId;
    }

    public function setCategoryId(string $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function setProductId(string $productId): void
    {
        $this->productId = $productId;
    }


}
