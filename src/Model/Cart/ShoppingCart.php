<?php

namespace Horeca\MiddlewareCommonLib\Model\Cart;

use Horeca\MiddlewareCommonLib\Constants\DeliveryTimeChoice;
use Horeca\MiddlewareCommonLib\Constants\InvoiceType;
use Horeca\MiddlewareCommonLib\Model\Customer\Company;
use Horeca\MiddlewareCommonLib\Model\Customer\Customer;
use Horeca\MiddlewareCommonLib\Model\Customer\CustomerData;
use Horeca\MiddlewareCommonLib\Model\Customer\ExternalCustomer;
use Horeca\MiddlewareCommonLib\Model\Location\Address;
use Horeca\MiddlewareCommonLib\Model\Location\DeliveryCenter;
use Horeca\MiddlewareCommonLib\Model\Restaurant\Restaurant;
use Horeca\MiddlewareCommonLib\Model\Restaurant\RestaurantTable;
use Horeca\MiddlewareCommonLib\Model\Website\Website;
use JMS\Serializer\Annotation as Serializer;

class ShoppingCart
{
    /**
     * @Serializer\SerializedName("id")
     * @Serializer\Type("string")
     */
    private string $id;

    /**
     * @Serializer\SerializedName("external_id")
     * @Serializer\Type("string")
     */
    private ?string $externalProviderId;

    /**
     * @Serializer\SerializedName("order_no")
     * @Serializer\Type("string")
     */
    private string $orderNumber;

    /**
     * @Serializer\SerializedName("payment_method")
     * @Serializer\Type("string")
     */
    private string $paymentMethod;

    /**
     * @Serializer\SerializedName("total_price")
     * @Serializer\Type("float")
     */
    private float $totalPrice;

    /**
     * @Serializer\SerializedName("total_price_before_discount")
     * @Serializer\Type("float")
     */
    private ?float $totalPriceBeforeDiscount = null;

    /**
     * @Serializer\SerializedName("delivery_cost")
     * @Serializer\Type("float")
     */
    private float $shipping = 0;

    /**
     * @Serializer\SerializedName("status")
     * @Serializer\Type("string")
     */
    private string $status;

    /**
     * @Serializer\SerializedName("payment_status")
     * @Serializer\Type("string")
     */
    private ?string $paymentStatus = null;

    /**
     * @Serializer\SerializedName("delivery_method")
     * @Serializer\Type("string")
     */
    private string $deliveryMethod;

    /**
     * @Serializer\SerializedName("delivery_time_choice")
     * @Serializer\Type("string")
     */
    private string $deliveryTimeChoice = DeliveryTimeChoice::NOW;

    /**
     * @Serializer\SerializedName("delivery_address")
     * @Serializer\Type("Horeca\MiddlewareCommonLib\Model\Location\Address")
     */
    private ?Address $deliveryAddress = null;

    /**
     * @Serializer\SerializedName("customer")
     * @Serializer\Type("Horeca\MiddlewareCommonLib\Model\Customer\Customer")
     */
    private ?Customer $customer = null;

    /**
     * @Serializer\SerializedName("external_customer")
     * @Serializer\Type("Horeca\MiddlewareCommonLib\Model\Customer\ExternalCustomer")
     */
    private ?ExternalCustomer $externalCustomer = null;

    /**
     * @Serializer\SerializedName("entries")
     * @Serializer\Type("array<Horeca\MiddlewareCommonLib\Model\Cart\ShoppingCartEntry>")
     * @var ShoppingCartEntry[]|array
     */
    private array $entries = [];

    /**
     * @Serializer\SerializedName("invoice_type")
     * @Serializer\Type("string")
     */
    private ?string $invoiceType = InvoiceType::PERSON;

    /**
     * @Serializer\SerializedName("customer_company")
     * @Serializer\Type("Horeca\MiddlewareCommonLib\Model\Customer\Company")
     */
    private ?Company $customerCompany = null;

    /**
     * @Serializer\SerializedName("details")
     * @Serializer\Type("string")
     */
    private ?string $customerNotes = null;

    /**
     * @Serializer\SerializedName("discount")
     * @Serializer\Type("Horeca\MiddlewareCommonLib\Model\Cart\Discount")
     */
    private ?Discount $discount = null;

    /**
     * @Serializer\SerializedName("estimated_delivery_minutes")
     * @Serializer\Type("integer")
     */
    private ?int $estimatedDeliveryMinutes = null;

    /**
     * @Serializer\SerializedName("estimated_delivery_time")
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private ?\DateTime $estimatedDeliveryTime = null;

    /**
     * @Serializer\SerializedName("estimated_preparation_minutes")
     * @Serializer\Type("integer")
     */
    private ?int $estimatedPreparationMinutes = null;

    /**
     * @Serializer\SerializedName("estimated_preparation_time")
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private ?\DateTime $estimatedPreparationTime = null;

    /**
     * @Serializer\SerializedName("cuttlery_quantity")
     * @Serializer\Type("integer")
     */
    private int $cutleryQuantity = 0;

    /**
     * @Serializer\SerializedName("restaurant")
     * @Serializer\Type("Horeca\MiddlewareCommonLib\Model\Restaurant\Restaurant")
     */
    private Restaurant $restaurant;

    /**
     * @Serializer\SerializedName("delivery_center")
     * @Serializer\Type("Horeca\MiddlewareCommonLib\Model\Location\DeliveryCenter")
     */
    private ?DeliveryCenter $deliveryCenter = null;

    /**
     * @Serializer\SerializedName("shopping_cart_tips")
     * @Serializer\Type("array<Horeca\MiddlewareCommonLib\Model\Cart\ShoppingCartTips>")
     * @var ShoppingCartTips[]|array
     */
    private array $shoppingCartTips = [];

    /**
     * @Serializer\SerializedName("restaurant_table")
     * @Serializer\Type("Horeca\MiddlewareCommonLib\Model\Restaurant\RestaurantTable")
     */
    private ?RestaurantTable $table = null;

    /**
     * @Serializer\SerializedName("website")
     * @Serializer\Type("Horeca\MiddlewareCommonLib\Model\Website\Website")
     */
    private Website $website;

    /**
     * @Serializer\SerializedName("time")
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private \DateTime $createdAt;

    /**
     * @Serializer\SerializedName("delivery_code")
     * @Serializer\Type("string")
     */
    private ?string $deliveryCode = '';

    /**
     * @Serializer\SerializedName("fees")
     * @Serializer\Type("array<Horeca\MiddlewareCommonLib\Model\Cart\FeeEntry>")
     * @var FeeEntry[]|array
     */
    private array $fees = [];

    /**
     * @Serializer\SerializedName("discounts")
     * @Serializer\Type("array<Horeca\MiddlewareCommonLib\Model\Cart\Discount>")
     * @var Discount[]|array
     */
    private array $discounts = [];

    public function __toString()
    {
        return $this->orderNumber;
    }

    //<editor-fold desc="Getters & Setters">

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getOrderNumber(): string
    {
        return $this->orderNumber;
    }

    public function setOrderNumber(string $orderNumber): void
    {
        $this->orderNumber = $orderNumber;
    }

    public function getPaymentMethod(): string
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(string $paymentMethod): void
    {
        $this->paymentMethod = $paymentMethod;
    }

    public function getTotalPrice(): float
    {
        return $this->totalPrice;
    }

    public function setTotalPrice(float $totalPrice): void
    {
        $this->totalPrice = $totalPrice;
    }

    public function getTotalPriceBeforeDiscount(): ?float
    {
        return $this->totalPriceBeforeDiscount;
    }

    public function setTotalPriceBeforeDiscount(?float $totalPriceBeforeDiscount): void
    {
        $this->totalPriceBeforeDiscount = $totalPriceBeforeDiscount;
    }

    public function getShipping()
    {
        return $this->shipping;
    }

    public function setShipping($shipping): void
    {
        $this->shipping = $shipping;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function getPaymentStatus(): ?string
    {
        return $this->paymentStatus;
    }

    public function setPaymentStatus(?string $paymentStatus): void
    {
        $this->paymentStatus = $paymentStatus;
    }

    public function getDeliveryMethod(): string
    {
        return $this->deliveryMethod;
    }

    public function setDeliveryMethod(string $deliveryMethod): void
    {
        $this->deliveryMethod = $deliveryMethod;
    }

    public function getDeliveryTimeChoice(): string
    {
        return $this->deliveryTimeChoice;
    }

    public function setDeliveryTimeChoice(string $deliveryTimeChoice): void
    {
        $this->deliveryTimeChoice = $deliveryTimeChoice;
    }

    public function getDeliveryAddress(): ?Address
    {
        return $this->deliveryAddress;
    }

    public function setDeliveryAddress(?Address $deliveryAddress): void
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): void
    {
        $this->customer = $customer;
    }

    public function getCustomerData(): CustomerData
    {
        if ($this->getCustomer()) {
            return new CustomerData($this->getCustomer()->getFullName(), $this->getCustomer()->getPhoneNumber());
        } else if ($this->getExternalCustomer()) {
            return new CustomerData($this->getExternalCustomer()->getName(), $this->getExternalCustomer()->getPhoneNumber());
        }
        return new CustomerData('N/A', 'N/A');

    }

    public function getEntries(): array
    {
        return $this->entries;
    }

    public function setEntries(array $entries): void
    {
        $this->entries = $entries;
    }

    public function addEntry(ShoppingCartEntry $entry)
    {
        $this->entries[] = $entry;
    }

    public function getInvoiceType(): ?string
    {
        return $this->invoiceType;
    }

    public function setInvoiceType(?string $invoiceType): void
    {
        $this->invoiceType = $invoiceType;
    }

    public function getCustomerCompany(): ?Company
    {
        return $this->customerCompany;
    }

    public function setCustomerCompany(?Company $customerCompany): void
    {
        $this->customerCompany = $customerCompany;
    }

    public function getCustomerNotes(): ?string
    {
        return $this->customerNotes;
    }

    public function setCustomerNotes(?string $customerNotes): void
    {
        $this->customerNotes = $customerNotes;
    }

    public function getDiscount(): ?Discount
    {
        return $this->discount;
    }

    public function setDiscount(?Discount $discount): void
    {
        $this->discount = $discount;
    }

    public function getEstimatedDeliveryMinutes(): ?int
    {
        return $this->estimatedDeliveryMinutes;
    }

    public function setEstimatedDeliveryMinutes(?int $estimatedDeliveryMinutes): void
    {
        $this->estimatedDeliveryMinutes = $estimatedDeliveryMinutes;
    }

    public function getEstimatedDeliveryTime(): ?\DateTime
    {
        return $this->estimatedDeliveryTime;
    }

    public function setEstimatedDeliveryTime(?\DateTime $estimatedDeliveryTime): void
    {
        $this->estimatedDeliveryTime = $estimatedDeliveryTime;
    }

    public function getCutleryQuantity(): int
    {
        return $this->cutleryQuantity;
    }

    public function setCutleryQuantity(int $cutleryQuantity): void
    {
        $this->cutleryQuantity = $cutleryQuantity;
    }

    public function getRestaurant(): Restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(Restaurant $restaurant): void
    {
        $this->restaurant = $restaurant;
    }

    public function getDeliveryCenter(): ?DeliveryCenter
    {
        return $this->deliveryCenter;
    }

    public function setDeliveryCenter(?DeliveryCenter $deliveryCenter): void
    {
        $this->deliveryCenter = $deliveryCenter;
    }

    public function getShoppingCartTips(): array
    {
        return $this->shoppingCartTips;
    }

    public function setShoppingCartTips(array $shoppingCartTips): void
    {
        $this->shoppingCartTips = $shoppingCartTips;
    }

    public function getTable(): ?RestaurantTable
    {
        return $this->table;
    }

    public function setTable(?RestaurantTable $table): void
    {
        $this->table = $table;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getEstimatedPreparationMinutes(): ?int
    {
        return $this->estimatedPreparationMinutes;
    }

    public function setEstimatedPreparationMinutes(?int $estimatedPreparationMinutes): void
    {
        $this->estimatedPreparationMinutes = $estimatedPreparationMinutes;
    }

    public function getEstimatedPreparationTime(): ?\DateTime
    {
        return $this->estimatedPreparationTime;
    }

    public function setEstimatedPreparationTime(?\DateTime $estimatedPreparationTime): void
    {
        $this->estimatedPreparationTime = $estimatedPreparationTime;
    }

    /**
     * @return Website
     */
    public function getWebsite(): Website
    {
        return $this->website;
    }

    /**
     * @param Website $website
     */
    public function setWebsite(Website $website): void
    {
        $this->website = $website;
    }

    //</editor-fold>

    /**
     * @return string|null
     */
    public function getDeliveryCode(): ?string
    {
        return $this->deliveryCode;
    }

    /**
     * @param string|null $deliveryCode
     */
    public function setDeliveryCode(?string $deliveryCode): void
    {
        $this->deliveryCode = $deliveryCode;
    }

    /**
     * @return string|null
     */
    public function getExternalProviderId(): ?string
    {
        return $this->externalProviderId;
    }

    /**
     * @param string|null $externalProviderId
     */
    public function setExternalProviderId(?string $externalProviderId): void
    {
        $this->externalProviderId = $externalProviderId;
    }

    /**
     * @return ExternalCustomer|null
     */
    public function getExternalCustomer(): ?ExternalCustomer
    {
        return $this->externalCustomer;
    }

    /**
     * @param ExternalCustomer|null $externalCustomer
     */
    public function setExternalCustomer(?ExternalCustomer $externalCustomer): void
    {
        $this->externalCustomer = $externalCustomer;
    }

    /**
     * @return array
     */
    public function getFees(): array
    {
        return $this->fees;
    }

    /**
     * @param array $fees
     */
    public function setFees(array $fees): void
    {
        $this->fees = $fees;
    }

    /**
     * @return array
     */
    public function getDiscounts(): array
    {
        return $this->discounts;
    }

    /**
     * @param array $discounts
     */
    public function setDiscounts(array $discounts): void
    {
        $this->discounts = $discounts;
    }

}
