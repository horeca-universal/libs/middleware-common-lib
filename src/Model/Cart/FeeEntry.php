<?php

namespace Horeca\MiddlewareCommonLib\Model\Cart;

use Horeca\MiddlewareCommonLib\Model\Fee\Fee;
use JMS\Serializer\Annotation as Serializer;

class FeeEntry
{

    /**
     * @Serializer\SerializedName("id")
     * @Serializer\Type("string")
     */
    private $id;

    /**
     * @Serializer\SerializedName("code")
     * @Serializer\Type("string")
     */
    private $code;

    /**
     * @Serializer\SerializedName("name")
     * @Serializer\Type("string")
     */
    private $descriptipn;

    /**
     * @Serializer\SerializedName("value")
     * @Serializer\Type("integer")
     */
    private $value;

    /**
     * @Serializer\SerializedName("parent_fee")
     * @Serializer\Type("Horeca\MiddlewareCommonLib\Model\Fee\Fee")
     */
    private ?Fee $parentFee = null;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getDescriptipn()
    {
        return $this->descriptipn;
    }

    /**
     * @param mixed $descriptipn
     */
    public function setDescriptipn($descriptipn): void
    {
        $this->descriptipn = $descriptipn;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }

    public function getParentFee(): ?Fee
    {
        return $this->parentFee;
    }

    public function setParentFee(?Fee $parentFee): void
    {
        $this->parentFee = $parentFee;
    }


}
