<?php

namespace Horeca\MiddlewareCommonLib\Model\Cart;

use JMS\Serializer\Annotation as Serializer;

class ShoppingCartTips
{
    /**
     * @Serializer\SerializedName("id")
     * @Serializer\Type("string")
     */
    private string $id;

    /**
     * @Serializer\SerializedName("amount")
     * @Serializer\Type("float")
     */
    private float $amount;

    /**
     * @Serializer\SerializedName("payment_status")
     * @Serializer\Type("string")
     */
    private string $paymentStatus;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    public function getPaymentStatus(): string
    {
        return $this->paymentStatus;
    }

    public function setPaymentStatus(string $paymentStatus): void
    {
        $this->paymentStatus = $paymentStatus;
    }

}
