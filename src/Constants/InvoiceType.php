<?php

namespace Horeca\MiddlewareCommonLib\Constants;

final class InvoiceType
{
    public const PERSON = 'person';
    public const COMPANY = 'company';

    private function __construct() { }
}
