<?php

namespace Horeca\MiddlewareCommonLib\Constants;

final class PaymentType
{
    public const CASH = 'cash';
    public const CREDIT_CARD = 'credit-card';
    public const CREDIT_CARD_ONLINE = 'credit-card-online';
    public const ON_ROOM = 'on-room';
    public const COMPLIMENTARY = 'complimentary';

    private function __construct() { }
}
