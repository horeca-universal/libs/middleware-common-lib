<?php

namespace Horeca\MiddlewareCommonLib\Constants;

final class DeliveryType
{
    public const DELIVERY = 'delivery';
    public const PICKUP = 'pickup';
    public const LOCAL_TABLE = 'local-table';

    private function __construct() { }
}
