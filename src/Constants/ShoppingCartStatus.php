<?php

namespace Horeca\MiddlewareCommonLib\Constants;

final class ShoppingCartStatus
{
    public const UNFINISHED = 'unfinished';     /* When the client didn't finish the order */
    public const RECEIVED = 'received';         /* When the order needs to be taken by an admin */
    public const PENDING = 'pending';           /* When the order waits to be saved in expressoft automatically */
    public const TAKEN = 'taken';               /* When the order is taken by an admin and waits to set a delivery time */
    public const ACCEPTED = 'accepted';         /* When the order is with fixed time delivery and it is accepted to be delivered at the requested hour */
    public const PREPARATION = 'preparation';   /* This is just for external orders! When the external order is ready to set a preparation time for the food */
    public const DONE = 'done';                 /* When the order is fully processed */
    public const CANCELED = 'canceled';         /* When the order is canceled by any reason */
    public const STATUS_IMPORTED = 'imported';         /* When the order is imported from external system */

    private function __construct() { }
}
