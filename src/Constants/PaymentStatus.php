<?php

namespace Horeca\MiddlewareCommonLib\Constants;

final class PaymentStatus
{
    public const SUBMITTED = 'submitted';   // payment needs to be processed at a later time
    public const WAITING = 'waiting';       // pending verification from payment service
    public const STARTED = 'started';       // payment process is initialized
    public const SUCCESS = 'success';       // payment completed with success
    public const REFUNDED = 'refunded';     // payment was refunded
    public const FAILED = 'failed';         // payment completed with error
    public const CANCELED = 'canceled';     // payment is revoked by the client
    public const DECLINED = 'declined';     // payment is declined by the service

    private function __construct() { }
}
