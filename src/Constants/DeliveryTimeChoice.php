<?php

namespace Horeca\MiddlewareCommonLib\Constants;

final class DeliveryTimeChoice
{
    public const NOW = 'now';
    public const IN_ONE_HOUR = 'one-hour';
    public const IN_TWO_HOURS = 'two-hours';
    public const AT_EXACT_TIME = 'exact-time';

    private function __construct() { }
}
