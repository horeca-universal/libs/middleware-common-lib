<?php

namespace Horeca\MiddlewareCommonLib\Constants;

final class DiscountType
{
    public const FIXED = 'fixed';
    public const PERCENT = 'percent';

    private function __construct() { }
}
