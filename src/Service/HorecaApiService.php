<?php

namespace Horeca\MiddlewareCommonLib\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Horeca\MiddlewareCommonLib\Exception\HorecaException;
use Horeca\MiddlewareCommonLib\Model\Cart\ShoppingCart;
use Horeca\MiddlewareCommonLib\Model\Protocol\SendShoppingCartResponse;
use Horeca\MiddlewareCommonLib\Model\Protocol\ShoppingCartProductMappingsResponse;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

class HorecaApiService implements HorecaApiInterface
{
    protected SerializerInterface $serializer;
    protected ?LoggerInterface $logger = null;

    private ?string $baseUrl = null;
    private ?string $apiKey = null;
    private ?string $middlewareClientId = null;
    private ?Client $client = null;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function setBaseUrl(string $baseUrl): void
    {
        $this->baseUrl = $baseUrl;
    }

    public function setApiKey(string $apiKey): void
    {
        $this->apiKey = $apiKey;
    }

    public function setMiddlewareClientId(string $middlewareClientId): void
    {
        $this->middlewareClientId = $middlewareClientId;
    }

    public function setLogger(?LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * @throws HorecaException
     */
    public function getShoppingCartProductMappings(ShoppingCart $cart): array
    {
        try {
            if (!$cart->getId()) {
                throw new HorecaException('Missing API parameter: cart.id');
            }

            $uri = sprintf('/middleware/cart/%s/product-mappings', $cart->getId());
            $response = $this->getClient()->get($uri);

            /** @var ShoppingCartProductMappingsResponse $data */
            $data = $this->serializer->deserialize($response->getBody()->getContents(), ShoppingCartProductMappingsResponse::class, 'json');

            return $data->getMappings();
        } catch (GuzzleException|\Exception $e) {
            throw new HorecaException($e->getMessage());
        }
    }

    /**
     * @throws HorecaException
     */
    public function confirmProviderNotified(ShoppingCart $cart): bool
    {
        try {
            if (!$cart->getId()) {
                throw new HorecaException('Missing API parameter: cart.id');
            }

            $uri = sprintf('/middleware/cart/%s/confirm-provider-notified', $cart->getId());
            $response = $this->getClient()->post($uri);

            return $response->getStatusCode() === Response::HTTP_OK;
        } catch (GuzzleException|\Exception $e) {
            throw new HorecaException($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function sendShoppingCart(ShoppingCart $cart, $restaurantId): SendShoppingCartResponse
    {
        try {
            $uri = sprintf('/middleware/order/%s', $restaurantId);

            $response = $this->getClient()->post($uri, [
                'json' => json_decode($this->serializer->serialize($cart, 'json'), true)
            ]);

            return $this->serializer->deserialize($response->getBody()->getContents(), SendShoppingCartResponse::class, 'json');

        } catch (GuzzleException|\Exception $e) {
            throw new HorecaException($e->getMessage());
        }
    }

    /**
     * @throws HorecaException
     */
    private function getClient(): Client
    {
        $this->validateCredentials();

        if (!$this->client) {
            $this->client = new Client([
                'base_uri' => $this->baseUrl,
                'headers'  => [
                    'Api-Key'             => $this->apiKey,
                    'X-Middleware-Client' => $this->middlewareClientId
                ],
                'timeout'  => 15
            ]);
        }

        return $this->client;
    }

    /**
     * @throws HorecaException
     */
    private function validateCredentials()
    {
        if (!($this->baseUrl && $this->apiKey && $this->middlewareClientId)) {
            throw new HorecaException('Invalid API credentials provided');
        }
    }
}
