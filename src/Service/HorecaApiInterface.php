<?php

namespace Horeca\MiddlewareCommonLib\Service;

use Horeca\MiddlewareCommonLib\Model\Cart\ShoppingCart;
use Horeca\MiddlewareCommonLib\Model\Product\ProductMapping;
use Horeca\MiddlewareCommonLib\Model\Protocol\SendShoppingCartResponse;

interface HorecaApiInterface
{
    /**
     * Returns the corresponding product mappings for each ShoppingCartEntry in the given ShoppingCart
     *
     * @return ProductMapping[]
     */
    public function getShoppingCartProductMappings(ShoppingCart $cart): array;

    /**
     * Sends a notification to horeca service when the ShoppingCart is sent to an external provider
     */
    public function confirmProviderNotified(ShoppingCart $cart): bool;

    /**
     * Sends a request to save the given shopping cart configuration
     */
    public function sendShoppingCart(ShoppingCart $cart, $restaurantId): SendShoppingCartResponse;
}
