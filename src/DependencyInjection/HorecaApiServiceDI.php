<?php

namespace Horeca\MiddlewareCommonLib\DependencyInjection;

use Horeca\MiddlewareCommonLib\Service\HorecaApiInterface;

trait HorecaApiServiceDI
{
    protected HorecaApiInterface $horecaApiService;

    /**
     * @required
     */
    public function setHorecaApiService(HorecaApiInterface $horecaApiService)
    {
        $this->horecaApiService = $horecaApiService;
    }
}
